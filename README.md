# Brainfuck

[Brainfuck](https://en.wikipedia.org/wiki/Brainfuck) is an [esoteric programming language](https://en.wikipedia.org/wiki/Esoteric_programming_language)
that's designed not to be easy to use but rather the opposite, as a
challenge to programmers.

This is simply my attempt to make an interpreter for the language.

This interpreter executes the "Hello World!" program as it should, but
there are no other guarantees.

The program is read from standard input, until end-of-file.
