#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <cstdint>

namespace
{
    void jump_forward(auto& pc, auto end)
    {
        for (; pc < end && *pc != ']'; ++pc)
        {
            if (*pc == '[')
            {
                jump_forward(++pc, end);
            }
        }
    }

    void jump_backward(auto& pc, auto begin)
    {
        for (; pc >= begin && *pc != '['; --pc)
        {
            if (*pc == ']')
            {
                jump_backward(--pc, begin);
            }
        }
    }
}

int main()
{
    // First read the input from standard input into a string
    std::string commands;
    
    std::string line;
    while (std::getline(std::cin, line))
    {
        commands += line;
    }

    // Filter out non-command characters
    auto i = std::remove_if(begin(commands), end(commands), [](char const& ch)
    {
        return !(ch == '<' || ch == '>' ||
                 ch == '+' || ch == '-' ||
                 ch == '.' || ch == ',' ||
                 ch == '[' || ch == ']');
        
    });
    if (i != end(commands))
    {
        commands.erase(i);
    }

    // The data vector, start out with one default-initialized element
    std::vector<std::int8_t> data{{}};
    
    auto current = begin(data);     // Current data position
    
    for (auto pc = begin(commands); pc != end(commands); ++pc)
    {
        switch (*pc)
        {
        case '<':
            if (current != begin(data))
            {
                --current;
            }
            break;

        case '>':
            if (current + 1 == end(data))
            {
                data.push_back({});
                current = end(data) - 1;
            }
            else
            {
                ++current;
            }
            break;

        case '+':
            *current += 1;
            break;

        case '-':
            *current -= 1;
            break;

        case '.':
            std::cout << static_cast<char>(*current);
            break;

        case ',':
        {
            char ch;
            std::cin >> ch;
            *current = static_cast<int8_t>(ch);
            break;
        }

        case '[':
            if (*current == 0)
            {
                jump_forward(++pc, end(commands));
            }
            break;

        case ']':
            if (*current != 0)
            {
                jump_backward(--pc, begin(commands));
            }
            break;

        default:
            // SHOULD NOT HAPPEN!
            break;
        }
    }
    
    return 0;
}
